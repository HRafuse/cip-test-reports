# Custom CIP test reports

This project features an automated pipeline that generates custom CIP reports
based on the results fetched from the [CIP SQUAD
instance](https://squad.ciplatform.org). The reports are published to GitLab
pages corresponding to this project:
<https://cip-playground.gitlab.io/squad-hacking/cip-test-reports>.

## Implementation

This project features a [docker
container](https://gitlab.com/cip-playground/squad-hacking/cip-test-reports/container_registry/)
that provides all tools required to fetch the data and generate the reports.

Inside the docker container, the following steps are performed:

- Using [squad-client](https://github.com/Linaro/squad-client), the required
  data is fetched from the [CIP SQUAD instance](https://squad.ciplatform.org).
- An overview is created from the data in the form of a markdown string using
  [python-tabulate](https://github.com/astanin/python-tabulate).
- An HTML version of the overview is generate using
  [python-markdown2](https://github.com/trentm/python-markdown2), which is
  embedded into an HTML template that uses the CSS stylesheet from
  [github-markdown-css](https://github.com/sindresorhus/github-markdown-css/).
- An index page is generated using the [tree
  command](http://mama.indstate.edu/users/ice/tree/).
- The generated files are uploaded to the [project's GitLab
  pages](https://cip-playground.gitlab.io/squad-hacking/cip-test-reports).

## Proof of concept

The current implementation is a proof of concept: it merely generates an
overview of all incomplete test jobs in the [CIP
Kernel](https://squad.ciplatform.org/cip-kernel) SQUAD project
[linux-6.1.y-cip](https://squad.ciplatform.org/cip-kernel/linux-6.1.y-cip/).

## License

An Apache style license applies to this project. Please refer to the
[LICENSE](LICENSE) file for more details.