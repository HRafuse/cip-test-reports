""" SQUAD client fetcher module """
import datetime as dt
import ast


def shorten_link(link):
    """ Create markdown link named as string behind the last slash """
    if link.endswith('/'):
        link = link[:-1]
    last = link.split('/')[-1]
    return [link, last]

def fetch_project_jobs(project):
    """ Fetch all test jobs for a given project for last seven days """
    today = dt.datetime.now(dt.timezone.utc)
    seven_days_ago = today - dt.timedelta(days=7)
    for build in project.builds().values():
       
        formatted_date = dt.datetime.strptime(build.created_at.split('T')[0], '%Y-%m-%d')

        for job in build.testjobs().values():
            if (seven_days_ago.month, seven_days_ago.day) < (formatted_date.month, formatted_date.day) < (today.month, today.day):
                yield build, job


def fetch_incomplete_jobs(project):
    """ Iterable for incomplete jobs including error data """
    fetcher = fetch_project_jobs(project)

    no_entries = True
    for build, job in fetcher:
        if job.job_status != 'Incomplete':
            continue
        no_entries = False
        failure = {'error_msg': 'N/A', 'error_type': 'N/A'}
        if job.failure:
            failure = ast.literal_eval(job.failure)
        suite = job.name.split('_')[-1]
        yield {
            'Version': f'{build.version}',
            'SQUAD Job': shorten_link(job.url),
            'LAVA Job': shorten_link(job.external_url),
            'Environment': f'{job.environment}',
            'Suite': f'{suite}',
            'Error Message':failure['error_msg'],
            'Error Type':failure['error_type']
        }
    if no_entries: # to assure rendering for empty table
        yield (' ', )*7
