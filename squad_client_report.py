"""
Generate html tables of incomplete jobs for a given SQUAD groups
"""

import sys
import pathlib

import panel as pn
from bokeh.models.widgets.tables import HTMLTemplateFormatter

import pandas as pd

import datetime as dt
from squad_client.core.api import SquadApi
from squad_client.core.models import Squad

from fetcher import fetch_incomplete_jobs

pn.extension(design='material')
pn.extension('tabulator')

def format_link(url, inner_text):
    return f'<a href="{url}" target="_blank">{inner_text}</a>'

def data_table_view(squad_url, group_name, project_name):
    """ Generate an overview of incomplete jobs for a given SQUAD project """

    SquadApi.configure(url=squad_url)
    group = Squad().group(group_name)

    project = group.project(project_name)
    gen = fetch_incomplete_jobs(project)

    data = gen

    df = pd.DataFrame(data, columns=['Version', 'SQUAD Job', 'LAVA Job', 'Environment', 'Suite', 'Error Message', 'Error Type']).set_index('Version')

    """ Add custom job filters """

    job_filters = {
        'Version': {'type': 'input', 'func': 'like', 'placeholder': 'Build Version'},
        'SQUAD Job': {'placeholder': 'SQUAD Job ID'},
        'LAVA Job': {'type': 'input', 'func': 'like', 'placeholder': 'LAVA Job ID'},
        'Environment': {'type': 'input', 'func': 'like', 'placeholder': 'Environment Type'},
        'Suite': {'type': 'input', 'func': 'like', 'placeholder': 'Suite'},
        'Error Message': {'type': 'input', 'func': 'like', 'placeholder': 'Error Message'},
        'Error Type': {'type': 'input', 'func': 'like', 'placeholder': 'Test Type'}
    }
    
    """ Show max 50 results for tabulator table with showing only 4 results per page """
    filter_table = pn.widgets.Tabulator(
        df.iloc[:50],
        pagination='local', layout='fit_columns', page_size=4, sizing_mode='stretch_width',
        header_filters=job_filters,
        disabled=True,
        formatters={
            
            'SQUAD Job': HTMLTemplateFormatter(
                    template='<a href="<%= value[0] %>" target="_blank"><%= value[1] %></a>'
                ),
            'LAVA Job': HTMLTemplateFormatter(
                    template='<a href="<%= value[0] %>" target="_blank"><%= value[1] %></a>'
                ),
            'Environment': HTMLTemplateFormatter(
                template='<code class="c_code"><%= value %></code>'
            ),
            'Suite': HTMLTemplateFormatter(
                template='<code class="c_code"><%= value %></code>'
            )
            
        }
    )

    df.to_html(render_links=True)

    return filter_table

def incomplete_project_jobs(squad_url, output_dir, groups):
    """ Generate an overview of incomplete jobs for a given SQUAD project """
    SquadApi.configure(url=squad_url)
    today = dt.date.today()
    seven_days_ago = today - dt.timedelta(days=7)
    for group in groups:
        for project in Squad().group(group).projects().values():
            cip_template = pn.Column(pn.pane.HTML(f""" <h1> Incomplete Jobs from {seven_days_ago} to {today}</h1> """), data_table_view(squad_url, group, project_name=project.slug)) 
            cip_template.save(f'{output_dir}/incomplete_jobs_{group}_{project.slug}.html', embed=True)

if __name__ == "__main__":
    
    out = pathlib.Path().cwd()
    
    if len(sys.argv) > 1:
        out = pathlib.Path(sys.argv[1])
    
    incomplete_project_jobs(
        squad_url='https://squad.ciplatform.org/',
        output_dir=out,
        groups=('linux-cip', 'linux-stable-rc'),
    )
